# @pp_simone_colabufalo/library-test

## 0.5.0

### Minor Changes

- Bump version to trigger publish

## 0.4.0

### Minor Changes

- Bump version to test deploy

## 0.3.0

### Minor Changes

- Bump version

## 0.2.0

### Minor Changes

- 7a311cf: Added greetings function

## 0.1.0

### Minor Changes

- added numberFormatter and useNumberFormatter functions
