import { unref, computed } from 'vue'
import type { MaybeRef } from '@vueuse/shared'

export function useNumberFormatter(
  value: MaybeRef<number>,
  locale: MaybeRef<string>,
  options?: MaybeRef<Intl.NumberFormatOptions>
) {
  const formattedNumber = computed(() => {
    if (!unref(value) || !unref(locale)) return null
    return new Intl.NumberFormat(unref(locale), unref(options)).format(unref(value))
  })
  return {
    formattedNumber
  }
}

export function numberFormatter(value: number, locale: string, options?: Intl.NumberFormatOptions) {
  if (!value || !locale) {
    return null
  } else {
    return new Intl.NumberFormat(locale, options).format(value)
  }
}

export function greetings(name: string) {
  return `Hello ${name}`
}